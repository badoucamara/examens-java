# R�ponses aux exercices

## Examen 2016

**Exercice 1**

__Q1__:

a. Un bean est une classe Java qui doit poss�der les propri�t�s suivantes :

- elle doit impl�menter Serializable ;

- elle doit poss�der un constructeur vide, soit celui qui existe par d�faut, soit d�clar� explicitement ;

- elle doit exposer des propri�t�s, sous forme de paires getters / setters .

__Q3__:
La m�thode *equals()* permet de comparer deux objets, et notamment de savoir s'ils sont �gaux.

Pr�caution : Il faudra �galement surcharger la m�thode *hashcode()*.

__Q4__:

a. Pour emp�cher la construction d�une instance de la classe Movie de l�ext�rieur de cette classe, il faudrait changer la visibilit� du consteucteur de la classe Movie et la mettre en *private*.

b. La m�thode of() doit �tre une m�thode *statique*.

c. Nom de la m�thode of() : __????????__

__Q5__:

a. Les m�canismes standards pour savoir si un objet est � plus grand � qu�un autre sont :

- Impl�menter l'interface *Comparable*: Utilisation de la m�thode *compareTo()*

- L'interface fonctionnelle Comparator

b. Sachant que l'on ne veut pas modifier la classe Movie, on va comparer les objets de Movie en utilisant l'interface Comparator car pour utilser la m�thode compareTo(), il va falloir la surcharger dans la classe Movie et il faut que cette derni�re impl�mente l'interface Comparable.

__Q8__:

a. La m�thode lines() de la classe BufferedReader permet de cr�er un stream de cha�nes de caract�res dont chaque �l�ment est une ligne de ce fichier.

b. On a 14129 �l�ments dans le stream. On peut v�rifier si ce nombre est correct en le comparant avec le ombre de lignes du fichier. 

__Q9__:

a. Problem : Not found, maybe line 14038

***

**Exercice 2**

__Q5__: 
Les 4 types de Collection sont: 
- List
- Set
- Map 
- Queue et Deque


__Q6__:
La collection ad�quate pour qu'il n'y ait pas de doublons et que les �l�ments soient maitenus tri�s est *SortedSet*.

__Q7__:

b. Pr�caution � prendre dans le cas o� certains acteurs n'ont pas de pr�nom: __????????__

***

## Examen 2017

**Exercice 2**

__Q1__:
On peut enregistrer ces noms dans un *SortedSet* afin de conserver l'ordre.


