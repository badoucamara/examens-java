package org.Camara_Alioune.examen2017.model;

public class Match {

	private Country countryA;
	private int scoreA;
	private Country countryB;
	private int scoreB;
	private String groupe;

	@Override
	public String toString() {
		return "Match [ " + countryA.getName() + "-" + countryB.getName() + ", score=" + scoreA + "-" + scoreB + "]";
	}

	public Match() {
	}

	public Country getCountryA() {
		return countryA;
	}

	public void setCountryA(Country countryA) {
		this.countryA = countryA;
	}

	public int getScoreA() {
		return scoreA;
	}

	public void setScoreA(int scoreA) {
		this.scoreA = scoreA;
	}

	public Country getCountryB() {
		return countryB;
	}

	public void setCountryB(Country countryB) {
		this.countryB = countryB;
	}

	public int getScoreB() {
		return scoreB;
	}

	public void setScoreB(int scoreB) {
		this.scoreB = scoreB;
	}

	public String getGroupe() {
		return groupe;
	}

	public void setGroupe(String groupe) {
		this.groupe = groupe;
	}

}
