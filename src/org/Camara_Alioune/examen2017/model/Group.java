package org.Camara_Alioune.examen2017.model;

import java.util.ArrayList;
import java.util.List;

public class Group {

	private String group;
	private List<Country> countries = new ArrayList<Country>();

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	public Group(String group) {
		this.setGroup(group);
	}

	public void addCountry(Country country) {
		this.countries.add(country);
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public List<Match> getMatches() {
		List<Match> matches = new ArrayList<>();
		for(Country c:countries)
		{
			
			int i =countries.indexOf(c)+1 ;
		
			while(i<countries.size()) {
			
				Match match =new Match();
				match.setCountryA(c);
				match.setCountryB(countries.get(i));
				matches.add(match);
				i++;
			};
			
		}

		return matches;

	}

}
