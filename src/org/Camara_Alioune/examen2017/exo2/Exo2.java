package org.Camara_Alioune.examen2017.exo2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import org.Camara_Alioune.examen2017.model.Country;
import  org.Camara_Alioune.examen2017.exo1.CountryAnalyzer;

public class Exo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Function<Country, String> countryToName = c -> c.getName();

		Comparator<Country> cmpAbc = Comparator.comparing(countryToName);

		CountryAnalyzer ca = new CountryAnalyzer();
		List<Country> listCountries = ca.readCountries("files/data.txt");
		listCountries.sort(cmpAbc);
		System.out.println("Countries from Group A : " + getCountryNamesFromGroup("A", listCountries));
		System.out.println("Opponents of Russie : " + getOpponents("Russie", listCountries));
		System.out.println("Opponents of Mexique : " + getOpponents("Mexique", listCountries));

	}

	static public  List<String> getCountryNamesFromGroup(String group, List<Country> countries) {
		List<String> list = new ArrayList<>();
		for (Country c : countries)
			if (c.getGroup().equals(group))
				list.add(c.getName());
		return list;

	}

	static public List<String> getOpponents(String countryName, List<Country> countries) {
		List<String> listOpponents = new ArrayList<>();
		for (Country c : countries) {
			if (c.getName().equals(countryName)) {
				listOpponents = getCountryNamesFromGroup(c.getGroup(), countries);
				listOpponents.remove(countryName);
				return listOpponents;
			}

		}
		return listOpponents;
	}

}
