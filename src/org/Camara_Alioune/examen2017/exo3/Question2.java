package org.Camara_Alioune.examen2017.exo3;

import java.util.ArrayList;
import java.util.List;

import org.Camara_Alioune.examen2017.exo1.CountryAnalyzer;
import org.Camara_Alioune.examen2017.model.Country;
import org.Camara_Alioune.examen2017.model.Group;
import org.Camara_Alioune.examen2017.model.Match;

public class Question2 {
	public static void main(String[] args) {

		CountryAnalyzer ca = new CountryAnalyzer();
		List<Country> listCountries = ca.readCountries("files/data.txt");
		System.out.println(listCountries);
		
		List<Group> groups = getGroups(listCountries);
		groups.forEach(g -> System.out.println(g.getGroup() + g.getCountries()));

		Country c1 = new Country("Usa", "A");
		Country c2 = new Country("Fr", "A");
		Country c3 = new Country("Sn", "A");
		Country c4 = new Country("Ger", "A");
		List<Country> countries = List.of(c1, c2, c3, c4);
		Group gr = new Group("A");
		gr.setCountries(countries);
		gr.getMatches().forEach(m -> System.out.println(m));
		
		getAllMatches(groups).forEach(m->System.out.println(m));;
		System.out.println(getAllMatches(groups).size());
	}

	static public List<Group> getGroups(List<Country> countries) {

		List<Group> groups = new ArrayList<>();

		for (Country c : countries) {
			int i = 0;
			if (groups.isEmpty()) {
				Group group = new Group(c.getGroup());
				group.addCountry(c);
				groups.add(group);

			}

			else {
				for (Group g : groups) {
					if (g.getGroup().equals(c.getGroup()))
						g.addCountry(c);
					else
						i++;
				}
				if (i == groups.size()) {

					Group group = new Group(c.getGroup());
					group.addCountry(c);
					groups.add(group);

				}

			}

		}

		return groups;

	}
	
	
	static List<Match>getAllMatches(List<Group> groups){
		List<Match> matches = new ArrayList<>();
		for(Group g:groups)
		{
			List<Match> m = g.getMatches();
			matches.addAll(m);
		}
		
		
		return matches;
		
	}

}
