package org.Camara_Alioune.examen2017.exo1;

import java.util.function.Function;
import java.util.function.UnaryOperator;

import org.Camara_Alioune.examen2017.model.Country;

public class Question5 {
	static UnaryOperator<String> lineToGroup = l-> l.split(" ")[0];
	static UnaryOperator<String> lineToTeamName = l-> l.split(" ")[1];
	static Function<String,Country> lineToCountry = l->{
		Country c = new Country();
		c.setGroup(lineToGroup.apply(l));
		c.setName(lineToTeamName.apply(l));
		return c;
		
	};
	public static void main(String[] args) {
		
		System.out.println(lineToCountry.apply("A RUSSIE"));
		

	}
}
