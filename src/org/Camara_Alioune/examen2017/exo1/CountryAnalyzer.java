package org.Camara_Alioune.examen2017.exo1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.Camara_Alioune.examen2017.model.Country;

public class CountryAnalyzer {
	
	public List<Country> readCountries(String filename) {
		List<Country> countries = new ArrayList<>();
		File file = new File(filename);
		try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr);){
	
			
			String line =br.readLine();
			
			while(line != null) {
				if((!line.startsWith("#"))&&(!line.isBlank())) {
					Country country = Question5.lineToCountry.apply(line);
					countries.add(country);				}
					
				line =br.readLine();
				}
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return countries;
		
	}

}
