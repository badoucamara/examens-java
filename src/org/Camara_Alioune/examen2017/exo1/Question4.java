package org.Camara_Alioune.examen2017.exo1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import org.Camara_Alioune.examen2017.model.Country;

public class Question4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Country c1 = new Country();
		Country c2 = new Country();
		Country c3 = new Country();

		c1.setName("Allemagne");
		c2.setName("Espagne");
		c3.setName("France");

		Function<Country, String> countryToName = c -> c.getName();
		Comparator<Country> cmpAbc = Comparator.comparing(countryToName);

		List<Country> countries = new ArrayList<>();
		countries.add(c2);
		countries.add(c3);
		countries.add(c1);
		
		System.out.println("Avant tri : " + countries);
		countries.sort(cmpAbc);
		System.out.println("Apr�s tri : " + countries);

	}

}
