package org.Camara_Alioune.examen2016.exo2.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Function;

public class Movie implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Function<Actor, String> actorToLastName = a -> a.getLastName();
	Function<Actor, String> actorToFirstName = a -> a.getFirstName();

	Comparator<Actor> compLastThenFirst = Comparator.comparing(actorToLastName)
			.thenComparing(Comparator.comparing(actorToFirstName));

	private String title;
	private int ReleaseYear;
	private SortedSet<Actor> actors = new TreeSet<Actor>(compLastThenFirst);

	private Movie() {
	}

	public static Movie of() {
		return new Movie();
	}

	private Movie(String title, int releaseYear) {
		// TODO Auto-generated constructor stub
		this.title = title;
		this.ReleaseYear = releaseYear;
	}

	public static Movie of(String title, int releaseYear) {

		return new Movie(title, releaseYear);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getReleaseYear() {
		return ReleaseYear;
	}

	public void setReleaseYear(int releaseYear) {
		ReleaseYear = releaseYear;
	}

	@Override
	public String toString() {
		return "Movie [title=" + title + ", ReleaseYear=" + ReleaseYear + ", actors=" + actors + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ReleaseYear;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (ReleaseYear != other.ReleaseYear)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	public SortedSet<Actor> getActors() {
		return actors;
	}



	public void addAllActors(SortedSet<Actor> actors) {
		this.actors.addAll(actors);

	}

}
