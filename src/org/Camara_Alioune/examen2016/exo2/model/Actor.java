package org.Camara_Alioune.examen2016.exo2.model;

import java.io.Serializable;

public class Actor implements Serializable,Comparable<Actor> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String firstName;
	private String lastName;

	private Actor() {

	}

	public static Actor of() {
		return new Actor();
	}

	private Actor(String lastName, String firstName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public static Actor of(String lineActor) {

		if (!lineActor.contains(","))
			return new Actor(lineActor, "");
		String lastName = lineActor.split(",")[0];
		String firstName = lineActor.split(",")[1].stripLeading();
		return new Actor(lastName, firstName);
	} 

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Actor [firstName=" + firstName + ", lastName=" + lastName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Actor other = (Actor) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}

	@Override
	public int compareTo(Actor o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
