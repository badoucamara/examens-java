package org.Camara_Alioune.examen2016.exo2.model;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MovieAnalyzer ma = new MovieAnalyzer();
		UnaryOperator<String> lineToNames = line -> line.substring(line.indexOf("/") + 1);
		System.out.println(lineToNames.apply(
				"8 Crazy Nights (2002)/Sandler, Adam/Sprouse, Cole/Sprouse, Dylan/Stout, Austin/Titone, Jackie"));

		Function<String, Stream<String>> lineToActorLines = line -> {
			String names[] = lineToNames.apply(line).split("/");
			List<String> listNames = Arrays.asList(names);

			return listNames.stream();

		};

		lineToActorLines
				.apply("8 Crazy Nights (2002)/Sandler, Adam/Sprouse, Cole/Sprouse, Dylan/Stout, Austin/Titone, Jackie")
				.forEach(a -> System.out.println(a));

		Set<Movie> movies = ma.readMovies("files/movies-mpaa.txt");
		movies.forEach(m->System.out.println(m));
		System.out.println(movies.size());
		
		

	}

}
