package org.Camara_Alioune.examen2016.exo2.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MovieAnalyzer {
	
	UnaryOperator<String> lineToTitleAndYear = line -> line.split("/")[0];

	UnaryOperator<String> lineToTitle = line -> lineToTitleAndYear.apply(line).split("\\(")[0].stripTrailing();
	
	Function<String, Integer> lineToYear = line -> Integer
			.parseInt(lineToTitleAndYear.apply(line).split("\\(")[1].substring(0, 4));

	Function<String, Movie> lineToMovie = line -> {
		Movie movie = Movie.of();
		movie.setTitle(lineToTitle.apply(line));
		movie.setReleaseYear(lineToYear.apply(line));
		return movie;
	};
	

	UnaryOperator<String> lineToNames = line -> line.substring(line.indexOf("/") + 1);
	Function<String, Stream<String>> lineToActorLines = line -> {
		String names[] = lineToNames.apply(line).split("/");
		List<String> listNames = Arrays.asList(names);

		return listNames.stream();

	};
	Function<String,Movie> lineToMovieWithActors = line->{
		Movie movie = lineToMovie.apply(line);
		SortedSet<Actor> actors = new TreeSet<Actor>();
		List<String> listActors = lineToActorLines.apply(line).collect(Collectors.toList());
		for(String s:listActors) {
			Actor a = Actor.of(s);
			actors.add(a);
		}
			movie.addAllActors(actors);
		
		return movie;
	};
	public Set<Movie> readMovies (String filename){
		Set<Movie> movies = new HashSet<Movie>();
		File file = new File(filename);
		try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr);){
			movies = br.lines().map(lineToMovieWithActors).collect(Collectors.toSet());
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return movies;
	}

}
