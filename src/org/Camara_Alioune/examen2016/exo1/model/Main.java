package org.Camara_Alioune.examen2016.exo1.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Movie m1 = Movie.of("Lush", 1999);
		Movie m2 = Movie.of("The Harvest", 1993);
		Movie m3 = Movie.of("Macbeth in Manhattan", 1999);

		Function<Movie, String> movieToTitle = m -> m.getTitle();
		Function<Movie, Integer> movieToYear = m -> m.getReleaseYear();

		Comparator<Movie> comp = Comparator.comparing(movieToYear).thenComparing(Comparator.comparing(movieToTitle));

		System.out.println(m1.getTitle() + " est plus ancien que " + m2.getTitle() + " : "
				+ (comp.compare(m1, m2) > 0 ? true : false));
		System.out.println(m1.getTitle() + " est plus ancien que " + m3.getTitle() + " : "
				+ (comp.compare(m1, m3) > 0 ? true : false));

		UnaryOperator<String> lineToTitleAndYear = line -> line.split("/")[0];

		UnaryOperator<String> lineToTitle = line -> lineToTitleAndYear.apply(line).split("\\(")[0].stripTrailing();

		Function<String, Integer> lineToYear = line -> Integer
				.parseInt(lineToTitleAndYear.apply(line).split("\\(")[1].substring(0, 4));

		Function<String, Movie> lineToMovie = line -> {
			Movie movie = Movie.of();
			movie.setTitle(lineToTitle.apply(line));
			movie.setReleaseYear(lineToYear.apply(line));
			return movie;
		};

		System.out.println(lineToMovie
				.apply("8 Crazy Nights (2002)/Sandler, Adam/Sprouse, Cole/Sprouse,Dylan/Stout, Austin/Titone, Jackie"));
		System.out.println(Main.read("files/movies-mpaa.txt").count());

		List<Movie> listMovies = Main.read("files/movies-mpaa.txt").map(lineToMovie).collect(Collectors.toList());
		System.out.println(listMovies.size());
	}

	public static Stream<String> read(String filename) {
		File file = new File(filename);
		List<String> lines = null;
		try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr);)

		{

			lines = br.lines().collect(Collectors.toList());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lines.stream();

	}
}